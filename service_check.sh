#!/bin/bash

email="bluescope.alerts@niiconsulting.com"
ip=`ip address show ens160 | grep -w inet | awk '{print $2}'`
service=$1
tmp_elastic=`ps -ef | grep elasticsearch | grep -v grep | grep -v controller | awk '{print $2}'`
echo $tmp_elastic
if [ -z $tmp_elastic ]
then
echo "elasticsearch master is not running"
echo "sending email"
/usr/bin/python /home/arpit/sendmail.py "elasticsearch master is not running on $ip server" "Restarting once again" $email
echo "Try To Start"
/usr/bin/python /home/arpit/sendmail.py "Try to start elasticsearch master  node on $ip" "Restarting once again" $email
/etc/init.d/elasticsearch start
sleep 5
tmp1=`ps -ef | grep elasticsearch | grep -v grep | grep -v controller | awk '{print $2}'`
if [ -z $tmp1 ]
then
	/usr/bin/python /home/arpit/sendmail.py "There is some problem with elasticsearch master on $ip Kindly check." "elastic master is not working" $email
else
	echo "Suceessfully Started"	
	/usr/bin/python /home/arpit/sendmail.py "elasticsearch master node on $ip is Successfully Started" "elasticsearch master working fine" $email
fi
else
echo "elasticsearch node master  is running"
fi

tmp_kibana=`ps -ef | grep kibana| grep -v grep | awk '{print $2}'`
echo $tmp_kibana
if [ -z $tmp_kibana ]
then
echo "kibana is not running"
echo "sending email"
/usr/bin/python /home/arpit/sendmail.py "kibana is not running on $ip server" "Restarting once again" $email
echo "Try To Start"
/usr/bin/python /home/arpit/sendmail.py "Try to start kibana  node on $ip" "Restarting once again" $email
service kibana start
sleep 5
tmp1=`ps -ef | grep kibana | grep -v grep | awk '{print $2}'`
if [ -z $tmp1 ]
then
        /usr/bin/python /home/arpit/sendmail.py "There is some problem with kibana on $ip Kindly check." "kibana is not working" $email
else
        echo "Suceessfully Started"
        /usr/bin/python /home/arpit/sendmail.py "kibana node on $ip is Successfully Started" "kibana working fine" $email
fi
else
echo "kibana is running"
fi
